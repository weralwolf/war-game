module.exports = function(grunt) {
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
    var APP_PORT = 9010;
    grunt.initConfig({
        connect: {
            all: {
                options: {
                    port: APP_PORT,
                    hostname: "0.0.0.0",
                    livereload: true,
                }
            }
        },
        open: {
            all: {
                path: 'http://localhost:<%= connect.all.options.port%>'
            }
        },
        watch: {
            all: {
                files: ['index.html', 'js/**.js'],
            }
        }
    });

    grunt.registerTask('server', [
        'connect',
        'open',
        'watch'
    ]);
};
