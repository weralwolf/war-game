# Foreword

Short idea of the zero-player game. There's two bases that spawn soldiers. Soldiers randomly walks around for their lifetime and setup bombs which could affect enemies if they are close enough. At the bottom of the board you can see statistics of fallen soldiers (killed by enemies bombs) and passed (soldiers died because their life time is excited).

# Install & run

To perform preparations before play you need to run:

```bower install```

And then open in browser index.html page. If you're lazy enough you can also do:

```npm install```

And then to run game just use:

```grunt server```

It will open browser tab for you.

# History
Clanging sound cry over dead desert and gates get opened. With a sound which will keep any living thing uneasy and force to run away as far as possible the dark opaque disk slides out from the factory. It stops for a while and scan the desert around. No alive enemies were detected, mission "Prevention measurements" activated, disk dissappears and soundlessly slides to setup bombs randomly and patrol the landscape.

No one knows how this war starts, because machines have no need to keep history of extincted civilizations. They are automated enough to reproduce them self and keep battle field alive. There's no longer commanders in front of triggers to stop the mess of explosions and insanity.
