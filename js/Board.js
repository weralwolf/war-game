var Board = (function() {
    'use strict';

    /**
     * Game config parameters.
     */
    var conf = {
        base: {
            productivity: 0.9,
        },
        soldier: {
            lifetime: 8000,
            bombity: 0.01,
            radius: 5,
        },
        bomb: {
            lifetime: 10,
            area: [-1, 0, 1],
        },
        board: {
            cell_size: 5,
        }
    };

    /** Unit present depictable object on board.
     * @param {SVGElement} svg refference to main board canvas.
     * @param {String} form the form of element to depict.
     * @param {Board} board refference to active board.
     * @param {String} army indentificator of fraction.
     * @param {Object} spawn pair of (x, y) coordinates in terms of board mesh.
     * @param {Object} options set of attributes to be assgined to graphical element as initial.
     */
    function Unit(svg, board, army, spawn, form, options) {
        this.svg = svg;
        this.id = utils.randID();

        this.board = board;
        this.army = army;
        this.place = spawn;

        this.obj = svg.append(form).attr('id', this.id);
        this.evolve(options);
    }

    /** Perform transformation of element attributes.
     * @param {Object} options set of options to be applied on graphical object.
     */
    Unit.prototype.evolve = function(options) {
        _.forIn(options, function(value, key) {
            this.obj.attr(key, value);
        }, this);
    };

    /** Remove element from board.
     */
    Unit.prototype.destroy = function() {
        d3.select('#' + this.id).remove();
        this.obj = null;
    };

    /** Create base of army, place where soldiers spawns.
     * @see Unit
     */
    function Base(board, army, spawn) {
        Unit.call(this,
            board.svg, board, army,
            spawn, 'rect', {
                'class': army + '-base',
                x: board.cell(spawn.x),
                y: board.cell(spawn.y),
                width: board.cell_size,
                height: board.cell_size,
            }
        );
    }
    Base.prototype = Object.create(Unit.prototype);
    Base.prototype.contructor = Unit;

    /** Update actions on timer tick.
     * @return {Boolean} does object is still alive.
     */
    Base.prototype.tick = function() {
        if (Math.random() < conf.base.productivity) {
            board.append(new Soldier(this.board, this.army, this.place));
        }
        return true; // forever alive
    };

    /** Bomb producer.
     */
    function Soldier(board, army, spawn) {
        Unit.call(this,
            board.svg, board, army, spawn,
            'circle', {
                'class': army + '-soldier',
                cx: board.cell(spawn.x, true),
                cy: board.cell(spawn.y, true),
                r: conf.soldier.radius,
            }
        );
        this.lifetime = conf.soldier.lifetime;
        this.alive = true;
    }
    Soldier.prototype = Object.create(Unit.prototype);
    Soldier.prototype.contructor = Unit;

    /** Update function for game tick.
     */
    Soldier.prototype.tick = function() {
        --this.lifetime;
        if (this.lifetime < 0) {
            return false;
        }
        this.place = {
            x: this.place.x + utils.randInt(-2, 3),
            y: this.place.y + utils.randInt(-2, 3),
        };
        if (this.place.x > this.board.width) {
            this.place.x -= this.board.width * 2;
        }

        if (this.place.x < 0) {
            this.place.x *= -1;
        }

        if (this.place.y > this.board.height) {
            this.place.y -= this.board.height * 2;
        }

        if (this.place.y < 0) {
            this.place.y *= -1;
        }
        this.evolve({
            cx: this.board.cell(this.place.x, true),
            cy: this.board.cell(this.place.y, true),
        });

        if (Math.random() < conf.soldier.bombity) {
            this.board.append(new Bomb(this.board, this.army, this.place));
        }
        return true;
    };

    function Bomb(board, army, spawn) {
        Unit.call(this,
            board.svg, board, army, spawn,
            'circle', {
                'class': army + '-bomb',
                cx: board.cell(spawn.x, true),
                cy: board.cell(spawn.y, true),
                r: board.cell_size * conf.bomb.area.length,
            }
        );
        this.lifetime = conf.bomb.lifetime;
    }
    Bomb.prototype = Object.create(Unit.prototype);
    Bomb.prototype.contructor = Unit;

    Bomb.prototype.tick = function() {
        --this.lifetime;
         if (this.lifetime < 0) {
            var explosion = _.chain(conf.bomb.area).map(function(dx) {
                return _.map(conf.bomb.area, function(dy) {
                    return {
                        x: this.place.x + dx,
                        y: this.place.y + dy,
                        army: this.army,
                    };
                }, this);
            }, this).flatten().value();
            this.board.endanger(explosion);
            return false;
         }
         return true;
    };

    /** Board - the space where soldiers and all other actors are moving.
     * @param {Integer} width - number of cells in a row.
     * @param {Integer} height - number of cells in column.
     */
    function Board(width, height) {
        this.width = width;
        this.height = height;

        var svg = this.svg = d3.select("body").append("svg")
            .attr("width", width * conf.board.cell_size)
            .attr("height", height * conf.board.cell_size)
        .append("g");

        svg.append('rect')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', width * conf.board.cell_size)
            .attr('height', height * conf.board.cell_size)
            .attr('class', 'board');

        this.actors = {};
        this.stats = {};
        this.counter_updated = false;
        this.danger = [];
    }

    /** Produce real coordinates of cell.
     * @param {Integer} a - number of cell (in row or in column, doesn't metters, they are square).
     * @param {Boolean} center - if `true` then center of cell will be calculated.
     *
     * @return {Number} coordinates of cell, in pixels.
     */
    Board.prototype.cell = function(a, center) {
        if (center === void(0)) {
            center = false;
        }
        return (a + 0.5 * center) * conf.board.cell_size;
    };

    /** Add new actor on the board.
     * @param {Unit} obj - an acting game object.
     */
    Board.prototype.append = function(obj) {
        this.actors[obj.id] = obj;
    };

    /** Inform board about list of fields where bombs were exploded and units should die.
     * @param {Object[]} fields list of triplets of ccordiantes and fraction bomb belongs to.
     */
    Board.prototype.endanger = function(fields) {
        this.danger.push(fields);
    };

    /** Check all endangered fields and kill units on them.
     */
    Board.prototype.kill = function() {
        _.chain(this.danger)
            .flatten()
            .unique()
            .forEach(function(explosion) {
                _.forEach(this.actors, function(actor) {
                    if (!actor.alive) {
                        return;
                    }
                    if (actor.place.x === explosion.x && actor.place.y == explosion.y && actor.army != explosion.army) {
                        ++this.stats[actor.army].fallen;
                        actor.destroy();
                        delete this.actors[actor.id];
                        this.counter_updated = false;
                    }
                }, this);
            }, this)
        .value();
        this.danger = [];
    };

    /** update board and all actors.
     */
    Board.prototype.update = function() {
        _.forEach(this.actors, function(actor) {
            var alive = actor.tick();
            if (!alive) {
                actor.destroy();
                if (actor.alive) {
                    ++this.stats[actor.army].passed;
                    this.counter_updated = false;
                }
                delete this.actors[actor.id];
            }
        }, this);

        this.kill();

        if (!this.counter_updated) {
            var content = "<table class='score'>";
            content += "<thead><tr><th>Army</th><th>Passed</th><th>Fallen</th></tr></thead><tbody>";
            _.forIn(this.stats, function(data, army) {
                content += "<tr><td>" + army + "</td><td>" + data.passed + "</td><td>" + data.fallen + "</td></tr>";
            });
            content += "</tbody></table>";
            document.getElementById('score').innerHTML = content;
            this.counter_updated = true;
        }
    };

    /** Produce base which belongs to specific fraction.
     * @param {String} army - name of fraction base belongs to.
     */
    Board.prototype.base = function(army) {
        this.append(new Base(this, army, {
            x: utils.randInt(0, board.width),
            y: utils.randInt(0, board.height),
        }));
        this.stats[army] = {
            fallen: 0,
            passed: 0,
        };
    };

    return Board;
})();
