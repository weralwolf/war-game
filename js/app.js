var board = new Board(150, 30);
board.base('red');
board.base('green');

var time = 0;
// Time difference between frames
var delay = 100;

// Live cycle of board
d3.timer(function(dt) {
    if (dt - time > delay) {
        time = dt;
        board.update();
    }
});
