var utils = (function() {
    'use strict';
    function utils() {}

    /** Produce random integer in range of `a` and `b`.
     * @param {Integer} a begin of range.
     * @param {Integer} b end of range.
     * @return {Integer} random integer in range between `a` and `b`.
     */
    utils.randInt = function(a, b) {
        return a + parseInt(Math.random() * (b - a));
    };

    /** Produce random sequence of letters to be used as ID.
     * @return {String} random identifier.
     */
    utils.randID = function () {
        return randi(10, {
            charset: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        });
    };

    return utils;
})();
